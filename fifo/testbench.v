`timescale 1ns/100ps

`include "fifo_cond.v"
`include "tester.v"
`include "cmos_cells.v"
`include "fifo_estruc.v"
`include "../memory/memory_cond.v"

module testbench ();

  //Se definen inputs:
  wire CLK;
  wire POP;
  wire PUSH;
  wire RESET_L;
  wire [4:0] FIFO_DATA_IN;
  wire [2:0] THRES_ALMOST_FULL;
  wire [2:0] THRES_ALMOST_EMPTY;


  //Se definen outputs:
  wire [4:0] FIFO_DATA_OUT;
  wire FIFO_ALMOST_EMPTY;
  wire FIFO_ALMOST_FULL;
  wire EMPTY;

  tester tester_
  (
    .clk(CLK),
    .reset_L(RESET_L),
    .fifo_data_in(FIFO_DATA_IN),
    .fifo_wr(PUSH),
    .fifo_rd(POP),
    .thres_almost_full(THRES_ALMOST_FULL),
    .thres_almost_empty(THRES_ALMOST_EMPTY)
  );

  fifo_cond fifo_cond_
  (
    .clk(CLK),
    .reset_L(RESET_L),
    .fifo_data_in(FIFO_DATA_IN),
    .fifo_wr(PUSH),
    .fifo_rd(POP),
    .fifo_data_out(FIFO_DATA_OUT),
    .fifo_almost_full(FIFO_ALMOST_FULL),
    .fifo_almost_empty(FIFO_ALMOST_EMPTY),
    .empty(EMPTY),
    .thres_almost_full(THRES_ALMOST_FULL),
    .thres_almost_empty(THRES_ALMOST_EMPTY)
  );

  fifo_estruc fifo_estruc_
  (
    .clk(CLK),
    .reset_L(RESET_L),
    .fifo_data_in(FIFO_DATA_IN),
    .fifo_wr(PUSH),
    .fifo_rd(POP),
    .fifo_data_out(FIFO_DATA_OUT),
    .fifo_almost_full(FIFO_ALMOST_FULL),
    .fifo_almost_empty(FIFO_ALMOST_EMPTY),
    .empty(EMPTY),
    .thres_almost_full(THRES_ALMOST_FULL),
    .thres_almost_empty(THRES_ALMOST_EMPTY)
  );

  initial begin
    //For GTKWave usage.
    $dumpfile("simulation.vcd");
    $dumpvars();
    #100 $finish();
  end

endmodule
