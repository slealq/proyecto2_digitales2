
module write_logic (
  input wire  clk,
  input wire  reset_L,
  input wire  fifo_wr,
  output reg  [2:0] wr_ptr
);

always @ (posedge clk) begin
  if (!reset_L) begin
    wr_ptr <= 0;
  end else begin
    if (fifo_wr) begin
      if (wr_ptr == 7) begin
        wr_ptr <= 0;
      end else begin
        wr_ptr <= wr_ptr + 1;
      end
    end
  end
end

endmodule

module read_logic (
  input wire  clk,
  input wire  reset_L,
  input wire  fifo_rd,
  output reg  [2:0] rd_ptr
);

always @ (posedge clk) begin
  if (!reset_L) begin
    rd_ptr <= 0;
  end else begin
    if (fifo_rd) begin
      if (rd_ptr == 7) begin
        rd_ptr <= 0;
      end else begin
        rd_ptr <= rd_ptr + 1;
      end
    end
  end
end

endmodule

module fifo_flag_control_logic (
  input wire  clk,
  input wire  reset_L,
  input wire  [2:0] wr_ptr,
  input wire  [2:0] rd_ptr,
  input wire  fifo_wr,
  input wire  fifo_rd,
  output reg  fifo_almost_full,
  output reg  fifo_almost_empty,
  // output reg  error
  output reg  errOverflow,
  output reg  errNoData,
  output reg  empty,
  // input umbrales almost full/empty
  input wire  [2:0] thres_almost_full,
  input wire  [2:0] thres_almost_empty
);

reg [3:0] ItemCounter;

// Condiciones para Overflow y NoData
always @ (posedge clk) begin
  if (!reset_L) begin
    ItemCounter <= 0;
    errNoData <= 0;
    errOverflow <= 0;
    fifo_almost_full <= 0;
    fifo_almost_empty <= 0;
    empty <= 1;
  end else begin
    if (fifo_wr) begin
      if (ItemCounter == 8) begin
        ItemCounter <= 8;
        errOverflow <= 1;
      end else begin
        ItemCounter <= ItemCounter + 1;
        errOverflow <= 0;
      end
    end

    if (fifo_rd) begin
      if (ItemCounter == 0) begin
        ItemCounter <= 0;
        errNoData <= 1;
      end else begin
        ItemCounter <= ItemCounter - 1;
        errNoData <= 0;
      end
    end

    if (fifo_wr && fifo_rd) begin
      ItemCounter <= ItemCounter;
    end

    // Condiciones para almost_empty y almost_full
    if (ItemCounter <= thres_almost_empty) begin
      fifo_almost_empty <= 1;
    end else if (ItemCounter >= thres_almost_full) begin
      fifo_almost_full <= 1;
    end else begin
      fifo_almost_empty <= 0;
      fifo_almost_full <= 0;
    end
    // empty
    if (ItemCounter == 0) begin
      empty <= 1;
    end else begin
      empty <= 0;
    end
  end
end


endmodule

module fifo_cond (
  input wire  clk,
  input wire  reset_L,
  // Puerto de escritura
  input wire  [4:0] fifo_data_in,
  input wire  fifo_wr,
  // Puerto de lectura
  output  [4:0] fifo_data_out,
  input wire  fifo_rd,
  // Puerto umbrales
  input wire  [2:0] thres_almost_full,
  input wire  [2:0] thres_almost_empty,
  // FIFO Flag control logic
  output  fifo_almost_full,
  output  fifo_almost_empty,
  output  empty);

  wire [2:0] rd_ptr;
  wire [2:0] wr_ptr;
  wire errNoData, errOverflow;


  read_logic READ (
    .clk(clk),
    .reset_L(reset_L),
    .fifo_rd(fifo_rd),
    .rd_ptr(rd_ptr)
  );

  write_logic WRITE (
    .clk(clk),
    .reset_L(reset_L),
    .fifo_wr(fifo_wr),
    .wr_ptr(wr_ptr)
  );

  fifo_flag_control_logic FLAG (
    .clk(clk),
    .reset_L(reset_L),
    .wr_ptr(wr_ptr),
    .rd_ptr(rd_ptr),
    .fifo_wr(fifo_wr),
    .fifo_rd(fifo_rd),
    .fifo_almost_full(fifo_almost_full),
    .fifo_almost_empty(fifo_almost_empty),
    .errOverflow(errOverflow),
    .errNoData(errNoData),
    .empty(empty),
    .thres_almost_full(thres_almost_full),
    .thres_almost_empty(thres_almost_empty)
  );

  memory_cond MEMO (
    .clk(clk),
    .reset_L(reset_L),
    .enable_wr(fifo_wr),
    .enable_rd(fifo_rd),
    .data_wr(fifo_data_in),
    .data_rd(fifo_data_out),
    .address_wr(wr_ptr),
    .address_rd(rd_ptr)
  );

endmodule

