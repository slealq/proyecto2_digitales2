module tester (
  output reg 	    clk,
  output reg 	    reset_L,
  output reg			[4:0] fifo_data_in,
  output reg      fifo_wr,
  output reg      fifo_rd,
  output reg      [2:0] thres_almost_full,
  output reg      [2:0] thres_almost_empty
);

  initial begin
    clk = 1;
    reset_L = 0;
    fifo_data_in = 0;
    fifo_rd = 0;
    fifo_wr = 0;
    thres_almost_full = 6;
    thres_almost_empty = 3;


    #3 reset_L = 1;


    @ (posedge clk);
    fifo_wr <= 1;
    fifo_data_in <= 5'h0A;

    @ (posedge clk);
    fifo_data_in <= 5'h1F;

    @ (posedge clk);
    fifo_data_in <= 5'h16;

    @ (posedge clk);
    fifo_data_in <= 5'h15;

    @ (posedge clk);
    fifo_data_in <= 5'h17;

    @ (posedge clk);
    fifo_data_in <= 5'h1B;

    @ (posedge clk);
    fifo_data_in <= 5'h19;

    @ (posedge clk);
    fifo_data_in <= 5'h1D;

    @ (posedge clk);
    fifo_data_in <= 5'h11;

    fifo_wr <= 0;
    fifo_rd <= 1;
    #12 fifo_wr <= 1;
    fifo_rd <= 0; 

    @ (posedge clk);
    fifo_data_in <= 5'h1B;

    @ (posedge clk);
    fifo_data_in <= 5'h00;

    @ (posedge clk);
    fifo_data_in <= 5'h1B;

    @ (posedge clk);
    fifo_data_in <= 5'h19;
    fifo_rd <= 1;
    #4 fifo_wr <= 0;

  end

  always begin
    #1 clk = ~clk;
  end

endmodule