module memory_cond (
  input wire  clk,
  input wire  reset_L,
  // Puerto de escritura
  input wire  enable_wr,
  input wire  [4:0] data_wr,
  input wire  [2:0] address_wr,
  // Puerto de lectura
  input wire  enable_rd,
  input wire  [2:0] address_rd,
  output reg  [4:0] data_rd
);

reg [4:0] MEMO[0:8]; // 8 words of 8-bit memory

integer i;

always @ (posedge clk) begin
  if (!reset_L) begin
    data_rd <= 0;
    for (i = 0; i < 8; i = i + 1) begin
      MEMO[i] = i;
    end
  end else begin
    if (enable_wr == 1'b1) begin
      MEMO[address_wr] <= data_wr;
    end 

    if (enable_rd == 1'b1) begin
      data_rd <= MEMO[address_rd];
    end else begin
      data_rd <= 0;
    end
  end
end

endmodule
