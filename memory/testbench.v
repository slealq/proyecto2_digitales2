`timescale 1ns/100ps

`include "memory_cond.v"
`include "tester.v"
`include "cmos_cells.v"
`include "memory_synth.v"

module testbench ();

  //Se definen inputs:
  wire CLK;
  wire ENABLE_WR;
  wire ENABLE_RD;
  wire RESET_L;
  wire [7:0] DATA_WR;
  wire [2:0] ADDRESS_WR;
  wire [2:0] ADDRESS_RD;

  //Se definen outputs:
  wire [7:0] DATA_RD;

  tester tester_
  (
    .clk(CLK),
    .reset_L(RESET_L),
    .enable_wr(ENABLE_WR),
    .enable_rd(ENABLE_RD),
    .data_wr(DATA_WR),
    .address_wr(ADDRESS_WR),
    .address_rd(ADDRESS_RD)
  );

  memory_cond memory_cond_
  (
    .clk(CLK),
    .reset_L(RESET_L),
    .enable_wr(ENABLE_WR),
    .enable_rd(ENABLE_RD),
    .data_wr(DATA_WR),
    .data_rd(DATA_RD),
    .address_wr(ADDRESS_WR),
    .address_rd(ADDRESS_RD)
  );

  memory_synth memory_synth_
  (
    .clk(CLK),
    .reset_L(RESET_L),
    .enable_wr(ENABLE_WR),
    .enable_rd(ENABLE_RD),
    .data_wr(DATA_WR),
    .data_rd(DATA_RD),
    .address_wr(ADDRESS_WR),
    .address_rd(ADDRESS_RD)
  );

  initial begin
    //For GTKWave usage.
    $dumpfile("simulation.vcd");
    $dumpvars();
    #35 $finish();
  end

endmodule
