module tester (
  output reg 	    clk,
  output reg 	    reset_L,
  output reg			enable_wr,
  output reg      [2:0] address_wr,
  output reg      enable_rd,
  output reg      [2:0] address_rd,
  output reg      [7:0] data_wr
);

  initial begin
    clk = 1;
    reset_L = 0;

    enable_rd = 0;
    enable_wr = 0;
    address_rd = 0;
    address_wr = 0;
    data_wr = 0;
    reset_L = 0;

    #3 reset_L = 1;


    @ (posedge clk);
    enable_wr <= 1;
    address_wr <= 5; 
    data_wr <= 5'h0A;

    // Escrtura en 4
    address_wr <= 4;
    @ (posedge clk);
    data_wr <= 5'h1F;

    // Escritura en 7
    address_wr <= 7;
    @ (posedge clk);
    data_wr <= 5'h15;

    // Escritura en 2
    address_wr <= 2;
    @ (posedge clk);
    data_wr <= 5'h17;
    
    // Escritura en 0
    address_wr <= 0;
    @ (posedge clk);
    data_wr <= 5'h1B;

    // Escritura en 1
    address_wr <= 1;
    @ (posedge clk);
    data_wr <= 5'h19;

    // Permitir lectura
    enable_rd <= 1;

    // Lectura en 1
    @ (posedge clk);
    address_rd <= 1;

    // Lectura en 2
    @ (posedge clk);
    address_rd <= 2;

    // Lectura en 7
    @ (posedge clk);
    address_rd <= 7;

    // Lectura en 4 y escritura en 3
    address_wr <= 3;
    @ (posedge clk);
    data_wr <= 5'h1D;
    address_rd <= 4; 

    // Lectura en 2 y escritura en 5
    address_wr <= 5;
    @ (posedge clk);
    data_wr <= 5'h03;
    address_rd <= 2; 

  end

  always begin
    #1 clk = ~clk;
  end

endmodule