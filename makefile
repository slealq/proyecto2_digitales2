##
# Proyecto 2
#
# @file
# @version 0.1

make sim_fsm :
	gtkwave fsm/fsm_sim.gtkw

make sim_memory :
	gtkwave memory/simulation.gtkw

make sim_round_robin :
	gtkwave round_robin/round_robin.gtkw

make sim_fifo :
	gtkwave fifo/simulation.gtkw

make sim_pcie :
	gtkwave pcie/pcie.gtkw

# end
