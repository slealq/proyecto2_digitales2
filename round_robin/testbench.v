`timescale 1ns / 100ps
`include "round_robin_cond.v"
`include "round_robin_est.v"
`include "cmos_cells.v"
`include "tester.v"

module testbench ();

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 clk;                    // From tst of tester.v
   wire                 input_selector_cond;    // From rrc of round_robin_cond.v
   wire                 input_selector_est;     // From rre of round_robin_est.v
   wire                 reset_L;                // From tst of tester.v
   wire                 vc0_in0_empty;          // From tst of tester.v
   wire                 vc0_in1_empty;          // From tst of tester.v
   wire                 vc1_in0_empty;          // From tst of tester.v
   wire                 vc1_in1_empty;          // From tst of tester.v
   wire                 vc_selector_cond;       // From rrc of round_robin_cond.v
   wire                 vc_selector_est;        // From rre of round_robin_est.v
   // End of automatics

   round_robin_cond rrc (
                         /*AUTOINST*/
                         // Outputs
                         .input_selector_cond   (input_selector_cond),
                         .vc_selector_cond      (vc_selector_cond),
                         // Inputs
                         .clk                   (clk),
                         .reset_L               (reset_L),
                         .vc0_in0_empty         (vc0_in0_empty),
                         .vc0_in1_empty         (vc0_in1_empty),
                         .vc1_in0_empty         (vc1_in0_empty),
                         .vc1_in1_empty         (vc1_in1_empty));

   round_robin_est rre (
                        /*AUTOINST*/
                        // Outputs
                        .input_selector_est(input_selector_est),
                        .vc_selector_est(vc_selector_est),
                        // Inputs
                        .clk            (clk),
                        .reset_L        (reset_L),
                        .vc0_in0_empty  (vc0_in0_empty),
                        .vc0_in1_empty  (vc0_in1_empty),
                        .vc1_in0_empty  (vc1_in0_empty),
                        .vc1_in1_empty  (vc1_in1_empty));

   tester tst (
               /*AUTOINST*/
               // Outputs
               .vc0_in0_empty           (vc0_in0_empty),
               .vc0_in1_empty           (vc0_in1_empty),
               .vc1_in0_empty           (vc1_in0_empty),
               .vc1_in1_empty           (vc1_in1_empty),
               .clk                     (clk),
               .reset_L                 (reset_L),
               // Inputs
               .input_selector_cond     (input_selector_cond),
               .input_selector_est      (input_selector_est),
               .vc_selector_cond        (vc_selector_cond),
               .vc_selector_est         (vc_selector_est));


endmodule
