module tester (
               input      input_selector_cond,
               input      input_selector_est,
               input      vc_selector_cond,
               input      vc_selector_est,
               output reg vc0_in0_empty,
               output reg vc0_in1_empty,
               output reg vc1_in0_empty,
               output reg vc1_in1_empty,
               output reg clk,
               output reg reset_L
               );

   initial begin

      $dumpfile("simulation.vcd");
      $dumpvars;

      clk <= 0;
      vc0_in0_empty <= 1;
      vc0_in1_empty <= 1;
      vc1_in1_empty <= 1;
      vc1_in0_empty <= 1;
      reset_L <= 0;

      // quitar reset
      #20 reset_L <= 1;

      #20;
      // set all vc to 1
      vc0_in0_empty <= 0;
      vc0_in1_empty <= 0;
      vc1_in0_empty <= 0;
      vc1_in1_empty <= 0;

      // now set only one or the other vc0 to 1
      #40;

      vc0_in1_empty <= 1;

      #20 vc0_in1_empty <= 0;
      vc0_in0_empty <= 1;

      // set both vc0 to 1
      #20 vc0_in0_empty <= 1;
      vc0_in1_empty <= 1;

      // set one vc1 to 0
      #20 vc1_in1_empty <= 1;

      #20 vc1_in1_empty <= 0;
      vc1_in0_empty <= 1;

      // set any of the vc0 to not empty
      #20 vc0_in0_empty <= 0;

      // end of tests
      #20 $finish;

   end // initial begin

   // verificador automático
   always @ (posedge clk) begin
      if (input_selector_est != input_selector_cond) begin
         $display("Error en el input selector a los %t", $realtime);
      end

      if (vc_selector_est != vc_selector_cond) begin
         $display("Error en el vc selector a los %t", $realtime);
      end
   end


   always begin
      #5 clk <= ~clk;
   end

endmodule
