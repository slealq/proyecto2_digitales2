module round_robin_cond (
                    input      clk,
                    input      reset_L,
                    input      vc0_in0_empty,
                    input      vc0_in1_empty,
                    input      vc1_in0_empty,
                    input      vc1_in1_empty,
                    output reg input_selector_cond, // decides input that goes to output
                    output reg vc_selector_cond,
                    output reg valid_cond
                    );

   wire                        one_vc0_isnotempty;
   wire                        both_vc0_arenotemtpy;
   wire                        only_vc0in1_isempty;
   wire                        both_vc1_arenotempty;
   wire                        only_vc1in1_isempty;

   assign one_vc0_isnotempty = vc0_in0_empty == 0 || vc0_in1_empty == 0;
   assign both_vc0_arenotemtpy = vc0_in0_empty == 0 && vc0_in1_empty == 0;
   assign only_vc0in1_isempty = vc0_in0_empty == 0 && ! (vc0_in1_empty == 0);
   assign both_vc1_arenotempty = vc1_in0_empty == 0 && vc1_in1_empty == 0;
   assign only_vc1in1_isempty = vc1_in0_empty == 0 && ! (vc1_in1_empty == 0);
   assign only_vc1in0_isempty = vc1_in1_empty == 0 && ! (vc1_in0_empty == 0);

   always @ (posedge clk) begin

      if (!reset_L) begin
         input_selector_cond <= 0;
         vc_selector_cond <= 0;
         valid_cond <= 0;
      end else begin
         // if both vc0 channels are NOT empty
         if (one_vc0_isnotempty) begin

            valid_cond <= 1;
            // select vc0 as output
            vc_selector_cond <= 0;
            // check if BOTH are NOT empty

            if (both_vc0_arenotemtpy) begin

               // if they are
               input_selector_cond <= ~input_selector_cond;
               // conmmute the signal between both

            end else begin
               // if they are NOT BOTH empty
               // check which is NOT empty
               if (only_vc0in1_isempty) begin

                  // select in0
                  input_selector_cond <= 0;

               end else begin

                  // at this point, is obvious that in1 is not empty
                  // select in1
                  input_selector_cond <= 1;

               end // else input_selector_cond

            end // else if one vc0 is not empty

         end else begin  // if vc0_in0_empty ...

            // in this case, since both vc0 are empty
            // then choose between both inputs
            // of vc1

            vc_selector_cond <= 1;

            if (both_vc1_arenotempty) begin

               // if both are empty, alternate between inputs
               input_selector_cond <= ~input_selector_cond;
               valid_cond <= 1;

            end else begin

               // check which is not empty
               if (only_vc1in1_isempty) begin

                  input_selector_cond <= 0;
                  valid_cond <= 1;

               end else begin

                  if (only_vc1in0_isempty) begin

                     input_selector_cond <= 1;
                     valid_cond <= 1;

                  end else begin
                     // no input is not empty
                     valid_cond <= 0;
                  end

               end

            end

         end // else: !if(both_vc1_areempty)

      end // else: !if(one_vc0_isnotempty)

   end // else !reset ...
endmodule
