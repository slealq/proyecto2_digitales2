# Instrucciones de simulación

Para visualizar las simulaciones en GTKWave, utilizar en la raíz principal:
- `make sim_memory` para la simulación de la memoria.
- `make sim_fsm` para la simulación de la máquina de estados. 
- `make sim_round_robin` para la simulación del round robin. 
- `make sim_fifo` para la simulación del fifo.
- `make sim_pcie` para la simulación del PCIE completa.

# 5 de noviembre

Descripción:
Crear repositorio e iniciar con el primer avance.
    
    
| Integrantes  | Tarea|Descripción  | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal  |  Repositorio | Creación de repositorio donde se guardará toda la información y documentos del proyecto   |30 min |
| Mariela Hernández  |  Memoria | Buscar información para creación de memoria   |30 min |

# 7 de noviembre

Descripción:
Primera reunión en donde se discute la estructura general del proyecto, roles de los miembros y  confección del primer avance.
    
    
| Integrantes | Tarea | Descripción | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Mariela Hernández  | Plan de trabajo  | Descripción del proyecto a ejecutar (roles de miembros y secciones) | 2 horas|
| Mariela Hernández  |  Bitácora | Seguimiento a las tareas que realiza  cada miembro  | 15 minutos |
| Mariela Hernández  |  Memoria | Código en verilog que permita describir la memoria   |2.5 horas |
| Stuart Leal  |  Máquina de Estados | Código en verilog que permita describir la máquina de estados   |2.5 horas |
| Pablo Hernández  |  Máquina de Estados | Código en verilog que permita describir la máquina de estados   |2.5 horas |


# 13 de noviembre

Descripción:
Avances del proyecto, se desarrollan parte de los objetivos que se deben entregar para la segunda semana.
    
    
| Integrantes | Tarea | Descripción | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Mariela Hernández  | FIFO  | Descripción conductual del módulo FIFO, y pruebas correspondientes | 4 horas|
| Stuart Leal |  Bitácora | Seguimiento a las tareas que realiza  cada miembro  | 15 minutos |
| Stuart Leal  |  Round Robin | Código en verilog que permita describir el módulo Round Robin | 4 horas |


# 19 de noviembre

Descripción:
Último avance del proyecto, respecto a código en Verilog.
    
    
| Integrantes | Tarea | Descripción | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal |  Interconexión completa | Se inicia el proceso de la interconexión completa de todos los módulos del proyecto  | 4 horas |
| Mariela Hernández  | FIFO  | Ligeros cambios en el conductual para agregar salidas de señales que se requieren 'afuera' del FIFO | 20 min |
| Mariela Hernández  |  Reporte | Seguimiento del reporte | 30 min |


# 20 de noviembre

Descripción:
Seguimiento de interconexión y reporte.
    
    
| Integrantes | Tarea | Descripción | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal |  Interconexión completa | Se continua el proceso de la interconexión completa de todos los módulos del proyecto  | 6 horas |
| Mariela Hernández  | FIFO  | Nuevamente pequeños cambios de salidas del FIFO | 20 min |
| Mariela Hernández  |  Reporte | Seguimiento del reporte | 2 horas |


# 21 de noviembre

Descripción:
Seguimiento de interconexión y reporte.
    
    
| Integrantes | Tarea | Descripción | Duración |
| :---------------: | :---------------:|:---------------:| :---------------:|
| Stuart Leal |  Interconexión completa | Se termina la interconexión completa de todos los módulos del proyecto  | 6 horas |
| Mariela Hernández  |  Reporte | Seguimiento del reporte | 2 horas |
