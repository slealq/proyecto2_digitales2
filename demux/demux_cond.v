// This should be only the conductual description
module demux_cond #(
                    parameter DATA_SIZE = 4
                    ) (
                       input                    clk,
                       input                    reset_L,
                       input                    selector,
                       input [DATA_SIZE:0]      data,
                       output reg [DATA_SIZE:0] data_out_0_cond,
                       output reg [DATA_SIZE:0] data_out_1_cond,
                       input                    valid,
                       output reg               valid_out_0_cond,
                       output reg               valid_out_1_cond
                       );

   wire [DATA_SIZE:0] temp_data_out_1_cond;
   wire [DATA_SIZE:0] temp_data_out_0_cond;

   assign temp_data_out_1_cond = ~(~data);
   assign temp_data_out_0_cond = ~(~data);

   always @ (posedge clk)
     begin
        // initial values
        if (!reset_L) begin
           // combinational logic
           data_out_0_cond <= 0;
           data_out_1_cond <= 0;
           valid_out_1_cond <= 0;
           valid_out_0_cond <= 0;

        end

        else begin
           if (selector) begin
              valid_out_1_cond <= valid;
              valid_out_0_cond <= 0;

              if (valid) begin
                 data_out_1_cond <= temp_data_out_1_cond;
              end
              else begin
                 data_out_1_cond <= data_out_1_cond;
              end
           end
           else begin
              valid_out_0_cond <= valid;
              valid_out_1_cond <= 0;

              if (valid) begin
                 data_out_0_cond <= temp_data_out_0_cond;
              end
              else begin
                 data_out_0_cond <= data_out_0_cond;
              end

           end
        end // if not reset

     end

endmodule
