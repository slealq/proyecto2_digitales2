module fsm_cond (
                 input wire       clk,
                 input wire       init,
                 input wire       reset_L,
                 input wire [2:0] umbral_vc_fc_low,
                 input wire [2:0] umbral_vc_fc_high,
                 input wire [3:0] not_empty, // bus de empty de todos los fifo
                 input wire [3:0] fifo_error,
                 output reg       error,
                 output reg       active,
                 output reg       idle,
                 output reg [2:0] umbrales_vc_fc_low,
                 output reg [2:0] umbrales_vc_fc_high
               );

   reg [4:0]       state;

   parameter [4:0] RESET = 5'b1, // 1
     INIT = 5'b10, // 2
     IDLE = 5'b100, // 4
     ACTIVE = 5'b1000, // 8
     ERROR = 5'b10000; // 1'0

   // logica de estados
   always @ (posedge clk) begin
      case (state)
        RESET :
          begin
             error <= 0;
             active <= 0;
             idle <= 0;
             umbrales_vc_fc_low <= umbrales_vc_fc_low;
             umbrales_vc_fc_high <= umbrales_vc_fc_high;

             if (!reset_L) begin
                state <= RESET;
             end else begin
                if (init) begin
                   state <= INIT;
                end
             end
          end

        INIT :
          begin
             error <= 0;
             active <= 0;
             idle <= 0;
             umbrales_vc_fc_low <= umbral_vc_fc_low;
             umbrales_vc_fc_high <= umbral_vc_fc_high;

             if (!reset_L) begin
                state <= RESET;
             end else begin
                if (init) begin
                   state <= INIT;
                end else begin
                   state <= IDLE;
                end
             end
          end // case: INIT

        IDLE :
          begin
             error <= 0;
             active <= 0;
             idle <= 1;
             umbrales_vc_fc_low <= umbrales_vc_fc_low;
             umbrales_vc_fc_high <= umbrales_vc_fc_high;

             if (!reset_L) begin
                state <= RESET;
             end else begin
                if (init) begin
                   state <= INIT;
                end else begin
                   if (|not_empty) begin // alguno está no vacío
                      state <= ACTIVE;
                   end else begin
                      state <= IDLE;
                   end
                end
             end // else: !if(!reset_L)
          end // case: IDLE

        ACTIVE :
          begin
             error <= 0;
             active <= 1;
             idle <= 0;
             umbrales_vc_fc_low <= umbrales_vc_fc_low;
             umbrales_vc_fc_high <= umbrales_vc_fc_high;

             if (!reset_L) begin
                state <= RESET;
             end else begin
                if (init) begin
                   state <= INIT;
                end else begin
                   if (|fifo_error) begin
                      state <= ERROR;
                   end else begin
                      state <= ACTIVE;
                   end // else umbral vc fc
                end // else init
             end // else !reset_L
          end // case: ACTIVE

        ERROR :
          begin
             error <= 1;
             active <= 0;
             idle <= 0;
             umbrales_vc_fc_low <= umbrales_vc_fc_low;
             umbrales_vc_fc_high <= umbrales_vc_fc_high;

             if (!reset_L) begin
                state <= RESET;
             end else begin
                state <= ERROR;
             end
          end // case: ERROR

        default :
          state <= RESET;

      endcase // case (state)

   end
endmodule
