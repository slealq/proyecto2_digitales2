
module fsm_tester(
                  output reg       clk,
                  output reg       reset_L,
                  output reg       init,
                  output reg [2:0] umbral_vc_fc_low,
                  output reg [2:0] umbral_vc_fc_high,
                  output reg [3:0] fifo_error,
                  output reg [3:0] not_empty
                  );

initial
  begin
     clk <= 0;
     reset_L <= 0;
     init <= 0;
     umbral_vc_fc_low <= 3;
     umbral_vc_fc_high <= 6;
     fifo_error <= 0;
     not_empty <= 0;
  end

initial
  begin
     $dumpfile("fsm_sim.vcd");
     $dumpvars;

     // prueba de estado init
     repeat (2) begin
        @(posedge clk);
     end

     reset_L <= 1;
     init <= 1;

     // prueba de estado idle
     @(posedge clk);

     init <= 0;

     // prueba de active
     repeat (4) begin
        @(posedge clk);
     end

     not_empty[3] <= 1;


     @(posedge clk);
     // prueba de error
     fifo_error[2] <= 1;

     repeat(5) begin
        @(posedge clk);
     end

     $finish;
  end

  always
    begin
      #5 clk <= ~clk;
    end

endmodule
