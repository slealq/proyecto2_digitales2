`timescale 1ns / 100ps
`include "fsm_cond.v"
`include "fsm_est.v"
`include "fsm_tester.v"
`include "cmos_cells.v"

module fsm_testbench;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 active;                 // From FSM_cond of fsm_cond.v, ...
   wire                 clk;                    // From FSM_prob of fsm_tester.v
   wire                 error_cond;             // From FSM_cond of fsm_cond.v
   wire                 error_est;              // From FSM_synth of fsm_est.v
   wire [3:0]           fifo_error;             // From FSM_prob of fsm_tester.v
   wire                 idle_cond;              // From FSM_cond of fsm_cond.v
   wire                 idle_est;               // From FSM_synth of fsm_est.v
   wire                 init;                   // From FSM_prob of fsm_tester.v
   wire [3:0]           not_empty;              // From FSM_prob of fsm_tester.v
   wire                 reset_L;                // From FSM_prob of fsm_tester.v
   wire [2:0]           umbral_vc_fc_high;      // From FSM_prob of fsm_tester.v
   wire [2:0]           umbral_vc_fc_low;       // From FSM_prob of fsm_tester.v
   wire                 umbrales_vc_fc_low_cond;// From FSM_cond of fsm_cond.v
   wire                 umbrales_vc_fc_low_est; // From FSM_synth of fsm_est.v
   wire                 umbrales_vc_fc_low_high_cond;// From FSM_cond of fsm_cond.v
   wire                 umbrales_vc_fc_low_high_est;// From FSM_synth of fsm_est.v
   // End of automatics

   /* fsm_cond AUTO_TEMPLATE (
    .error (error_cond),
    .idle (idle_cond),
    .umbrales_vc_fc_low (umbrales_vc_fc_low_cond),
    .umbrales_vc_fc_high (umbrales_vc_fc_low_high_cond),
    );
    */
   fsm_cond FSM_cond (
                      /*AUTOINST*/
                      // Outputs
                      .error            (error_cond),            // Templated
                      .active           (active),
                      .idle             (idle_cond),             // Templated
                      .umbrales_vc_fc_low(umbrales_vc_fc_low_cond), // Templated
                      .umbrales_vc_fc_high(umbrales_vc_fc_low_high_cond), // Templated
                      // Inputs
                      .clk              (clk),
                      .init             (init),
                      .reset_L          (reset_L),
                      .umbral_vc_fc_low (umbral_vc_fc_low[2:0]),
                      .umbral_vc_fc_high(umbral_vc_fc_high[2:0]),
                      .not_empty        (not_empty[3:0]),
                      .fifo_error       (fifo_error[3:0]));
   /* fsm_est AUTO_TEMPLATE (
    .error (error_est),
    .idle (idle_est),
    .umbrales_vc_fc_low (umbrales_vc_fc_low_est),
    .umbrales_vc_fc_high (umbrales_vc_fc_low_high_est),
    );
    */
   fsm_est FSM_synth (
                        /*AUTOINST*/
                      // Outputs
                      .active           (active),
                      .error            (error_est),             // Templated
                      .idle             (idle_est),              // Templated
                      .umbrales_vc_fc_high(umbrales_vc_fc_low_high_est), // Templated
                      .umbrales_vc_fc_low(umbrales_vc_fc_low_est), // Templated
                      // Inputs
                      .clk              (clk),
                      .fifo_error       (fifo_error[3:0]),
                      .init             (init),
                      .not_empty        (not_empty[3:0]),
                      .reset_L          (reset_L),
                      .umbral_vc_fc_high(umbral_vc_fc_high[2:0]),
                      .umbral_vc_fc_low (umbral_vc_fc_low[2:0]));

   fsm_tester FSM_prob (
                        /*AUTOINST*/
                        // Outputs
                        .clk            (clk),
                        .reset_L        (reset_L),
                        .init           (init),
                        .umbral_vc_fc_low(umbral_vc_fc_low[2:0]),
                        .umbral_vc_fc_high(umbral_vc_fc_high[2:0]),
                        .fifo_error     (fifo_error[3:0]),
                        .not_empty      (not_empty[3:0]));

endmodule //fsm_testbench
