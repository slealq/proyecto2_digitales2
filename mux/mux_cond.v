module mux_cond #(
                  parameter DATA_SIZE = 4
                  ) (
                     input            clk,
                     input            reset_L,
                     input [DATA_SIZE:0]      data_0,
                     input [DATA_SIZE:0]      data_1,
                     input            valid_0,
                     input            valid_1,
                     input            selector,
                     output reg       valid_out,
                     output reg [DATA_SIZE:0] data_out
                     );

   always @ (posedge clk)
     begin
        if (reset_L) begin
           if (!selector) begin
              if (valid_0) begin
                 data_out <= data_0;
                 valid_out <= valid_0;
              end else begin
                 valid_out <= 0;
              end
           end else begin
              if (valid_1) begin
                 data_out <= data_1;
                 valid_out <= valid_1;
              end else begin
                 valid_out <= 0;
              end
           end
        end else begin // if (reset_L)
           data_out <= 0;
           valid_out <= 0;
        end

     end

endmodule
