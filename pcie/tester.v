module tester (
               input [5:0]      output_port_0_cond,
               input [5:0]      output_port_1_cond,
               input [5:0]      output_port_0_est,
               input [5:0]      output_port_1_est,
               output reg       clk,
               output reg       reset_L,
               output reg [6:0] input_port_0,
               output reg [6:0] input_port_1,
               output reg       init
               );

   // los puertos de entrada se descomponen en 7 bits :
   // | - | - | - | ---- |
   //   v   i   d   data
   //
   // acá:
   //        v      es el valor de valid para el dato
   //        i      es el valor del vc_id al cuál va el dato
   //        d      es el detiny, se usa posteriormente
   //      data     los datos que entran
   reg                          v_in0, v_in1;
   reg                         i_in0, i_in1;
   reg                         d_in0, d_in1;
   reg [3:0]                   data_in0, data_in1;

   always @ (posedge clk) begin
      input_port_0 <= {v_in0, i_in0, d_in0, data_in0};
      input_port_1 <= {v_in1, i_in1, d_in1, data_in1};
   end

   initial begin
      $dumpfile("simulation.vcd");
      $dumpvars;

      // initial global values
      clk <= 0;
      reset_L <= 0;
      input_port_0 <= 0;
      input_port_1 <= 0;
      init <= 0;

      // first values for all entries
      v_in0 <= 0;
      i_in0 <= 0;
      d_in0 <= 0;
      data_in0 <= 0;

      v_in1 <= 0;
      i_in1 <= 0;
      d_in1 <= 0;
      data_in1 <= 0;

      repeat (2) begin
         @(posedge clk);
         init <=1 ;
      end

      reset_L <= 1;

      @(posedge clk);
      init <= 0;

      @(posedge clk);
      v_in0 <= 1;
      v_in1 <= 1;

      repeat (20) begin
         // for port 0
         // alternate vc id between 0 and 1
         i_in0 <= ~i_in0 ;
         // alternate destiny between 0 and 1
         d_in0 <= 1 ;
         // increase datain by 1
         data_in0 <= data_in0 + 1;

         // for port 1
         // alternate vc id between 0 and 1
         i_in1 <= ~i_in1 ;
         // alternate destiny between random values
         d_in1 <= 0 ;
         // decrease datain by 1
         data_in1 <= data_in1 - 1;
         @(posedge clk);
      end // repeat (20)

      // stop pushing data
      v_in0 <= 0;
      v_in1 <= 0;
      
      repeat (20) begin
         @(posedge clk);
      end

      $finish;

   end

   always begin
      #5 clk <= ~clk;
   end

endmodule
