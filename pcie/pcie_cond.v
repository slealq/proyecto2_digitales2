module pcie_cond #(parameter DATA_SIZE = 4,
                   parameter LOW_THRES = 3,
                   parameter HIGH_THRES = 6) (
                                              input            clk,
                                              input            reset_L,
                                              input            init,
                                              input [6:0]      input_port_0,
                                              input [6:0]      input_port_1,
                                              output [5:0] output_port_0_cond,
                                              output [5:0] output_port_1_cond,
                                              output error_fsm,
                                              output active_fsm,
                                              output idle_fsm,
                                              output [3:0] pause_bus,
                                              output [3:0] continue_bus
                                              );
   // los puertos de entrada se descomponen en 7 bits :
   // | - | - | - | ---- |
   //   v   i   d   data
   //
   // acá:
   //        v      es el valor de valid para el dato
   //        i      es el valor del vc_id al cuál va el dato
   //        d      es el detiny, se usa posteriormente
   //      data     los datos que entran


   wire [4:0]        vc0in0_input, vc1in0_input;
   wire              vc0in0_valid_input, vc1in0_valid_input;


   wire [4:0]        vc0in1_input, vc1in1_input;
   wire              vc0in1_valid_input, vc1in1_valid_input;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [5:0]           data_out;               // From mux_output of mux_cond.v
   wire [4:0]           data_out_vc0;           // From mux_vc0 of mux_cond.v
   wire [4:0]           data_out_vc1;           // From mux_vc1 of mux_cond.v
   wire                 empty_vc0in0;           // From fifo_condvc0in0 of fifo_cond.v
   wire                 empty_vc0in1;           // From fifo_condvc0in1 of fifo_cond.v
   wire                 empty_vc1in0;           // From fifo_condvc1in0 of fifo_cond.v
   wire                 empty_vc1in1;           // From fifo_condvc1in1 of fifo_cond.v
   wire                 fifo_almost_empty_vc0in0;// From fifo_condvc0in0 of fifo_cond.v
   wire                 fifo_almost_empty_vc0in1;// From fifo_condvc0in1 of fifo_cond.v
   wire                 fifo_almost_empty_vc1in0;// From fifo_condvc1in0 of fifo_cond.v
   wire                 fifo_almost_empty_vc1in1;// From fifo_condvc1in1 of fifo_cond.v
   wire                 fifo_almost_full_vc0in0;// From fifo_condvc0in0 of fifo_cond.v
   wire                 fifo_almost_full_vc0in1;// From fifo_condvc0in1 of fifo_cond.v
   wire                 fifo_almost_full_vc1in0;// From fifo_condvc1in0 of fifo_cond.v
   wire                 fifo_almost_full_vc1in1;// From fifo_condvc1in1 of fifo_cond.v
   wire [4:0]           fifo_data_out_vc0in0;   // From fifo_condvc0in0 of fifo_cond.v
   wire [4:0]           fifo_data_out_vc0in1;   // From fifo_condvc0in1 of fifo_cond.v
   wire [4:0]           fifo_data_out_vc1in0;   // From fifo_condvc1in0 of fifo_cond.v
   wire [4:0]           fifo_data_out_vc1in1;   // From fifo_condvc1in1 of fifo_cond.v
   wire                 input_selector_cond;    // From rrc of round_robin_cond.v
   wire [2:0]           thres_almost_empty;     // From fsm_cond_ of fsm_cond.v
   wire [2:0]           thres_almost_full;      // From fsm_cond_ of fsm_cond.v
   wire                 valid_cond;             // From rrc of round_robin_cond.v
   wire                 valid_out;              // From mux_output of mux_cond.v
   wire                 valid_vc0;              // From mux_vc0 of mux_cond.v
   wire                 valid_vc1;              // From mux_vc1 of mux_cond.v
   wire                 vc_selector_cond;       // From rrc of round_robin_cond.v
   // End of automatics

   wire [2:0]           uvfl;
   wire [2:0]           uvfh;

   reg                  valid_vc0in0;
   reg                  valid_vc1in0;
   reg                  valid_vc0in1;
   reg                  valid_vc1in1;

   reg                  temp_is;
   reg                  temp_vc_d;
   reg                  temp_vc_dd;
   
   // valores de los threshold
   assign uvfl = LOW_THRES;
   assign uvfh = HIGH_THRES;

   // valores de pause y continue de cada fifo
   assign pause_bus = {fifo_almost_full_vc0in0, fifo_almost_full_vc1in0, fifo_almost_full_vc0in1, fifo_almost_full_vc1in1};
   assign continue_bus = {fifo_almost_empty_vc0in0, fifo_almost_empty_vc1in0, fifo_almost_empty_vc0in1, fifo_almost_empty_vc1in1};
   

   // para el puerto de entrada 0
   demux_cond #(.DATA_SIZE (DATA_SIZE)) demux_in_0 (
                                                    .clk (clk),
                                                    .reset_L (reset_L),
                                                    .data (input_port_0 [4:0]),
                                                    .selector (input_port_0 [5]),
                                                    .valid (input_port_0 [6]),
                                                    .data_out_0_cond (vc0in0_input[4:0]),
                                                    .data_out_1_cond (vc1in0_input[4:0]),
                                                    .valid_out_0_cond (vc0in0_valid_input),
                                                    .valid_out_1_cond (vc1in0_valid_input)
                                                    );

   // para el puerto de entrada 1
   demux_cond #(.DATA_SIZE (DATA_SIZE)) demux_in_1 (
                                                    .clk (clk),
                                                    .reset_L (reset_L),
                                                    .data (input_port_1 [4:0]),
                                                    .selector (input_port_1 [5]),
                                                    .valid (input_port_1 [6]),
                                                    .data_out_0_cond (vc0in1_input[4:0]),
                                                    .data_out_1_cond (vc1in1_input[4:0]),
                                                    .valid_out_0_cond (vc0in1_valid_input),
                                                    .valid_out_1_cond (vc1in1_valid_input)
                                                    );

   // instantiate both vc0 and vc1 for input 0

   /* fifo_cond AUTO_TEMPLATE (
    .fifo_data_in (vc0in0_input),
    .fifo_wr (vc0in0_valid_input),
    .empty (empty_vc0in0),
    .fifo_rd (pop_vc0in0),
    .fifo_data_out (fifo_data_out_vc0in0[4:0]),
    .fifo_almost_full (fifo_almost_full_vc0in0),
    .fifo_almost_empty (fifo_almost_empty_vc0in0),
    );
    */
   fifo_cond fifo_condvc0in0 (/*AUTOINST*/
                              // Outputs
                              .fifo_data_out    (fifo_data_out_vc0in0[4:0]), // Templated
                              .fifo_almost_full (fifo_almost_full_vc0in0), // Templated
                              .fifo_almost_empty(fifo_almost_empty_vc0in0), // Templated
                              .empty            (empty_vc0in0),  // Templated
                              // Inputs
                              .clk              (clk),
                              .reset_L          (reset_L),
                              .fifo_data_in     (vc0in0_input),  // Templated
                              .fifo_wr          (vc0in0_valid_input), // Templated
                              .fifo_rd          (pop_vc0in0),    // Templated
                              .thres_almost_full(thres_almost_full[2:0]),
                              .thres_almost_empty(thres_almost_empty[2:0]));

   /* fifo_cond AUTO_TEMPLATE (
    .fifo_data_in (vc1in0_input),
    .fifo_wr (vc1in0_valid_input),
    .empty (empty_vc1in0),
    .fifo_rd (pop_vc1in0),
    .fifo_data_out (fifo_data_out_vc1in0[4:0]),
    .fifo_almost_full (fifo_almost_full_vc1in0),
    .fifo_almost_empty (fifo_almost_empty_vc1in0),
    );
    */
   fifo_cond fifo_condvc1in0 (/*AUTOINST*/
                              // Outputs
                              .fifo_data_out    (fifo_data_out_vc1in0[4:0]), // Templated
                              .fifo_almost_full (fifo_almost_full_vc1in0), // Templated
                              .fifo_almost_empty(fifo_almost_empty_vc1in0), // Templated
                              .empty            (empty_vc1in0),  // Templated
                              // Inputs
                              .clk              (clk),
                              .reset_L          (reset_L),
                              .fifo_data_in     (vc1in0_input),  // Templated
                              .fifo_wr          (vc1in0_valid_input), // Templated
                              .fifo_rd          (pop_vc1in0),    // Templated
                              .thres_almost_full(thres_almost_full[2:0]),
                              .thres_almost_empty(thres_almost_empty[2:0]));

   /* fifo_cond AUTO_TEMPLATE (
    .fifo_data_in (vc0in1_input),
    .fifo_wr (vc0in1_valid_input),
    .empty (empty_vc0in1),
    .fifo_rd (pop_vc0in1),
    .fifo_data_out (fifo_data_out_vc0in1[4:0]),
    .fifo_almost_full (fifo_almost_full_vc0in1),
    .fifo_almost_empty (fifo_almost_empty_vc0in1),
    );
    */
   fifo_cond fifo_condvc0in1 (/*AUTOINST*/
                              // Outputs
                              .fifo_data_out    (fifo_data_out_vc0in1[4:0]), // Templated
                              .fifo_almost_full (fifo_almost_full_vc0in1), // Templated
                              .fifo_almost_empty(fifo_almost_empty_vc0in1), // Templated
                              .empty            (empty_vc0in1),  // Templated
                              // Inputs
                              .clk              (clk),
                              .reset_L          (reset_L),
                              .fifo_data_in     (vc0in1_input),  // Templated
                              .fifo_wr          (vc0in1_valid_input), // Templated
                              .fifo_rd          (pop_vc0in1),    // Templated
                              .thres_almost_full(thres_almost_full[2:0]),
                              .thres_almost_empty(thres_almost_empty[2:0]));

   /* fifo_cond AUTO_TEMPLATE (
    .fifo_data_in (vc1in1_input),
    .fifo_wr (vc1in1_valid_input),
    .empty (empty_vc1in1),
    .fifo_rd (pop_vc1in1),
    .fifo_data_out (fifo_data_out_vc1in1[4:0]),
    .fifo_almost_full (fifo_almost_full_vc1in1),
    .fifo_almost_empty (fifo_almost_empty_vc1in1),
    );
    */
   fifo_cond fifo_condvc1in1 (/*AUTOINST*/
                              // Outputs
                              .fifo_data_out    (fifo_data_out_vc1in1[4:0]), // Templated
                              .fifo_almost_full (fifo_almost_full_vc1in1), // Templated
                              .fifo_almost_empty(fifo_almost_empty_vc1in1), // Templated
                              .empty            (empty_vc1in1),  // Templated
                              // Inputs
                              .clk              (clk),
                              .reset_L          (reset_L),
                              .fifo_data_in     (vc1in1_input),  // Templated
                              .fifo_wr          (vc1in1_valid_input), // Templated
                              .fifo_rd          (pop_vc1in1),    // Templated
                              .thres_almost_full(thres_almost_full[2:0]),
                              .thres_almost_empty(thres_almost_empty[2:0]));


   // Ahora hay que conectar la máuina de estados
   /* fsm_cond AUTO_TEMPLATE (
    .umbral_vc_fc_low (uvfl[2:0]),
    .umbral_vc_fc_high (uvfh[2:0]),
    .not_empty ({!empty_vc0in0, !empty_vc0in1, !empty_vc1in0, !empty_vc1in1}),
    .fifo_error ({fifo_almost_full_vc0in0, fifo_almost_full_vc0in1, fifo_almost_full_vc1in0, fifo_almost_full_vc1in1}),
    .umbrales_vc_fc_low (thres_almost_empty[2:0]),
    .umbrales_vc_fc_high (thres_almost_full[2:0]),
    .error (error_fsm),
    .active (active_fsm),
    .idle (idle_fsm),
    );
    */
   fsm_cond fsm_cond_ (/*AUTOINST*/
                       // Outputs
                       .error           (error_fsm),             // Templated
                       .active          (active_fsm),            // Templated
                       .idle            (idle_fsm),              // Templated
                       .umbrales_vc_fc_low(thres_almost_empty[2:0]), // Templated
                       .umbrales_vc_fc_high(thres_almost_full[2:0]), // Templated
                       // Inputs
                       .clk             (clk),
                       .init            (init),
                       .reset_L         (reset_L),
                       .umbral_vc_fc_low(uvfl[2:0]),             // Templated
                       .umbral_vc_fc_high(uvfh[2:0]),            // Templated
                       .not_empty       ({!empty_vc0in0, !empty_vc0in1, !empty_vc1in0, !empty_vc1in1}), // Templated
                       .fifo_error      ({fifo_almost_full_vc0in0, fifo_almost_full_vc0in1, fifo_almost_full_vc1in0, fifo_almost_full_vc1in1})); // Templated

   // Template para el round robin
   /* round_robin_cond AUTO_TEMPLATE (
    .vc0_in0_empty (empty_vc0in0),
    .vc0_in1_empty (empty_vc0in1),
    .vc1_in0_empty (empty_vc1in0),
    .vc1_in1_empty (empty_vc1in1),
    );
    */

   round_robin_cond rrc (/*AUTOINST*/
                         // Outputs
                         .input_selector_cond   (input_selector_cond),
                         .vc_selector_cond      (vc_selector_cond),
                         .valid_cond            (valid_cond),
                         // Inputs
                         .clk                   (clk),
                         .reset_L               (reset_L),
                         .vc0_in0_empty         (empty_vc0in0),  // Templated
                         .vc0_in1_empty         (empty_vc0in1),  // Templated
                         .vc1_in0_empty         (empty_vc1in0),  // Templated
                         .vc1_in1_empty         (empty_vc1in1));  // Templated

   // lógica para los pop
   // estas mismas señales se pueden usar como valid de las salidas, pero retrasada
   assign pop_vc0in0 = (!vc_selector_cond && !input_selector_cond && valid_cond);
   assign pop_vc1in0 = (vc_selector_cond && !input_selector_cond && valid_cond);
   assign pop_vc0in1 = (!vc_selector_cond && input_selector_cond && valid_cond);
   assign pop_vc1in1 = (vc_selector_cond && input_selector_cond && valid_cond);

   // valid de los muxes
   always @ (posedge clk) begin
      valid_vc0in0 <= pop_vc0in0;
      valid_vc1in0 <= pop_vc1in0;
      valid_vc0in1 <= pop_vc0in1;
      valid_vc1in1 <= pop_vc1in1;
   end

   // el input selector necesito atrasarlo un ciclo
   always @ (posedge clk) begin
      temp_is <= input_selector_cond;
   end

   // el vc selector tengo que atrasarlo dos ciclos
   always @ (posedge clk) begin
      temp_vc_d <= vc_selector_cond;
      temp_vc_dd <= temp_vc_d;
   end

   // muxes despues de los fifos
   /* mux_cond AUTO_TEMPLATE (
    .data_0 (fifo_data_out_vc0in0[4:0]),
    .data_1 (fifo_data_out_vc0in1[4:0]),
    .valid_0 (valid_vc0in0),
    .valid_1 (valid_vc0in1),
    .selector (temp_is),
    .valid_out (valid_vc0),
    .data_out (data_out_vc0[4:0]),
    );
    
    */
   mux_cond mux_vc0 (/*AUTOINST*/
                     // Outputs
                     .valid_out         (valid_vc0),             // Templated
                     .data_out          (data_out_vc0[4:0]),     // Templated
                     // Inputs
                     .clk               (clk),
                     .reset_L           (reset_L),
                     .data_0            (fifo_data_out_vc0in0[4:0]), // Templated
                     .data_1            (fifo_data_out_vc0in1[4:0]), // Templated
                     .valid_0           (valid_vc0in0),          // Templated
                     .valid_1           (valid_vc0in1),          // Templated
                     .selector          (temp_is));               // Templated
   
   
   /* mux_cond AUTO_TEMPLATE (
    .data_0 (fifo_data_out_vc1in0[4:0]),
    .data_1 (fifo_data_out_vc1in1[4:0]),
    .valid_0 (valid_vc1in0),
    .valid_1 (valid_vc1in1),
    .selector (temp_is),
    .valid_out (valid_vc1),
    .data_out (data_out_vc1[4:0]),
    );
    
    */
   mux_cond mux_vc1 (/*AUTOINST*/
                     // Outputs
                     .valid_out         (valid_vc1),             // Templated
                     .data_out          (data_out_vc1[4:0]),     // Templated
                     // Inputs
                     .clk               (clk),
                     .reset_L           (reset_L),
                     .data_0            (fifo_data_out_vc1in0[4:0]), // Templated
                     .data_1            (fifo_data_out_vc1in1[4:0]), // Templated
                     .valid_0           (valid_vc1in0),          // Templated
                     .valid_1           (valid_vc1in1),          // Templated
                     .selector          (temp_is));               // Templated
  
   // el siguiente mux elige entre un fifo o el otro
   /* mux_cond AUTO_TEMPLATE (
    .data_0 ({1'b0, data_out_vc0[4:0]}),
    .data_1 ({1'b1, data_out_vc1[4:0]}),
    .data_out (data_out[5:0]),
    .valid_0 (valid_vc0),
    .valid_1 (valid_vc1),
    .selector (temp_vc_dd),
    );
    */

   mux_cond #(.DATA_SIZE (5)) mux_output (/*AUTOINST*/
                                          // Outputs
                                          .valid_out            (valid_out),
                                          .data_out             (data_out[5:0]), // Templated
                                          // Inputs
                                          .clk                  (clk),
                                          .reset_L              (reset_L),
                                          .data_0               ({1'b0, data_out_vc0[4:0]}), // Templated
                                          .data_1               ({1'b1, data_out_vc1[4:0]}), // Templated
                                          .valid_0              (valid_vc0),     // Templated
                                          .valid_1              (valid_vc1),     // Templated
                                          .selector             (temp_vc_dd));    // Templated

   // demux final
   // TEMPLATE PARA EL DEMUX
   /* demux_cond AUTO_TEMPLATE (
    .data ({data_out[5], data_out[3:0]}),
    .valid (valid_out),
    .data_out_0_cond (output_port_0_cond[4:0]),
    .data_out_1_cond (output_port_1_cond[4:0]),
    .valid_out_0_cond (output_port_0_cond[5]),
    .valid_out_1_cond (output_port_1_cond[5]),
    .selector (data_out[4]),
    );
   */

   demux_cond demux_output (/*AUTOINST*/
                            // Outputs
                            .data_out_0_cond    (output_port_0_cond[4:0]), // Templated
                            .data_out_1_cond    (output_port_1_cond[4:0]), // Templated
                            .valid_out_0_cond   (output_port_0_cond[5]), // Templated
                            .valid_out_1_cond   (output_port_1_cond[5]), // Templated
                            // Inputs
                            .clk                (clk),
                            .reset_L            (reset_L),
                            .selector           (data_out[4]),   // Templated
                            .data               ({data_out[5], data_out[3:0]}), // Templated
                            .valid              (valid_out));     // Templated
     
endmodule // pcie_cond
// Local Variables:
// verilog-library-files:("../fifo/fifo_cond.v" "../fsm/fsm_cond.v" "../round_robin/round_robin_cond.v" "../mux/mux_cond.v" "../demux/demux_cond.v")
// End:
