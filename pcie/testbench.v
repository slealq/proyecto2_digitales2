`timescale 1ns / 100ps
`include "../demux/demux_cond.v"
`include "../mux/mux_cond.v"
`include "../fifo/fifo_cond.v"
`include "../memory/memory_cond.v"
`include "../fsm/fsm_cond.v"
`include "../round_robin/round_robin_cond.v"
`include "pcie_cond.v"
`include "pcie_est.v"
`include "tester.v"
`include "cmos_cells.v"

module testbench ();

  /*AUTOWIRE*/
  // Beginning of automatic wires (for undeclared instantiated-module outputs)
  wire                  clk;                    // From tst of tester.v
  wire                  init;                   // From tst of tester.v
  wire [6:0]            input_port_0;           // From tst of tester.v
  wire [6:0]            input_port_1;           // From tst of tester.v
  wire [5:0]            output_port_0_cond;     // From pciec of pcie_cond.v, ...
  wire [5:0]            output_port_1_cond;     // From pciec of pcie_cond.v, ...
  wire [5:0]            output_port_0_est;     // From pciec of pcie_est.v, ...
  wire [5:0]            output_port_1_est;     // From pciec of pcie_est.v, ...
  wire                  reset_L;                // From tst of tester.v
  // End of automatics

  pcie_cond pciec (/*AUTOINST*/
                   // Outputs
                   .output_port_0_cond  (output_port_0_cond[5:0]),
                   .output_port_1_cond  (output_port_1_cond[5:0]),
                   // Inputs
                   .clk                 (clk),
                   .reset_L             (reset_L),
                   .init                (init),
                   .input_port_0        (input_port_0[6:0]),
                   .input_port_1        (input_port_1[6:0]));

   pcie_est pciee (/*AUTOINST*/
                    // Outputs
                    .output_port_0_est (output_port_0_est[5:0]),
                    .output_port_1_est (output_port_1_est[5:0]),
                    // Inputs
                    .clk                (clk),
                    .reset_L            (reset_L),
                    .init               (init),
                    .input_port_0       (input_port_0[6:0]),
                    .input_port_1       (input_port_1[6:0]));


   tester tst (/*AUTOINST*/
               // Outputs
               .clk                     (clk),
               .reset_L                 (reset_L),
               .input_port_0            (input_port_0[6:0]),
               .input_port_1            (input_port_1[6:0]),
               .init                    (init),
               // Inputs
               .output_port_0_cond      (output_port_0_cond[5:0]),
               .output_port_1_cond      (output_port_1_cond[5:0]),
               .output_port_0_est      (output_port_0_est[5:0]),
               .output_port_1_est      (output_port_1_est[5:0])
               );

   // Local Varibles:
   // verilog-library-files:("pcie_est.v")
   // End:

endmodule
